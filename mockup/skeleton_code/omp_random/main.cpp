#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <omp.h>
#include <random>
#include <string>

// Integrand, g(x)
double integrand(double x)
{
    return x - 2;
}

// Function to minimize, f(x).
// Monte Carlo integration of `integrand(y)` for `0 < y < x` with M samples.
double integrate(double x, size_t M, std::mt19937& gen)
{
    if (x <= 0) {
        return 0;
    }
    double sum = 0;
    std::uniform_real_distribution<double> dist(0, x);
    for (size_t j = 0; j < M; ++j) {
        const double y = dist(gen);
        sum += integrand(y);
    }
    return x * sum / M;
}

// Minimizes the integral of `integrand(y)` for `0 < y < x`.
// N: number of random walks
// T: number of steps
// Mmax: maximum number of samples for integration
//
// Output:
// xmins: xmin for each step
// fmins: fmin for each step
// evals: number of evaluations of `integrand()` for each thread
void minimize(size_t N, size_t T, size_t Mmax,
              /*out*/
              std::vector<double>& xmins, std::vector<double>& fmins,
              std::vector<size_t>& evals)
{
    //
    // TODO.a
    // TODO.b
    //

    evals.resize(omp_get_max_threads());

    std::vector<double> xx(N);

    // initial positions
    for (size_t i = 0; i < N; ++i) {
        xx[i] = (i + 0.5) / N;
    }

    std::mt19937 gen;
    const double sigma = 1. / N;
    std::normal_distribution<double> normal(0., 1.);
    // steps
    for (size_t t = 0; t < T; ++t) {
        double xmin;
        double fmin = std::numeric_limits<double>::max();

        for (size_t i = 0; i < N; ++i) {
            const double x = xx[i];
            double f = 0;
            if (x > 0) {
                const size_t M = Mmax * x;
                evals[0] += M;
                f = integrate(x, M, gen);
            }
            if (f < fmin) {
                xmin = x;
                fmin = f;
            }
            xx[i] += sigma * normal(gen);
        }
        xmins.push_back(xmin);
        fmins.push_back(fmin);
    }
}

/* TODO.c

Write your answers to subquestion c) here:

* Evaluations per thread after running with 2 threads:


* Is the number of evaluations the same for all threads?
  Explain in one sentence why.


*/


int main(int argc, char** argv)
{
    const size_t N = 2000;     // number of random walks
    const size_t T = 20;       // number of steps
    const size_t Mmax = 20000; // maximum number of samples for integration

    std::vector<double> xmins;
    std::vector<double> fmins;
    std::vector<size_t> evals;
    minimize(N, T, Mmax, xmins, fmins, evals);

    std::cout << "\nSteps (xmin, fmin):\n";
    for (size_t t = 0; t < T; ++t) {
        std::cout << xmins[t] << " " << fmins[t] << std::endl;
    }
    std::cout << "\nEvaluations per thread (1e6):\n";
    for (size_t i = 0; i < evals.size(); ++i) {
        std::cout << "thread " << i << ": " << evals[i] / 1e6 << '\n';
    }
    std::cout << std::endl;

    return 0;
}
