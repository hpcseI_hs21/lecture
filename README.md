# TA Contacts

* Ermioni : <permioni@ethz.ch>
* Pascal  : <webepasc@ethz.ch>
* Daniel  : <wadaniel@ethz.ch>
* Johan   : <johan.lokna@math.ethz.ch>
* Pascal A.d.M. : <pascalau@student.ethz.ch>
* Man Hin : <chengman@student.ethz.ch>

# Exercise Hand-In

Please submit your exercise solutions via Moodle (nethz login required):

https://moodle-app2.let.ethz.ch/course/view.php?id=15876

# Frequently Asked Questions (FAQ)

Please have a look at these [Frequently asked questions](./faq/faq.md)


