#ifndef CALCULATOR_HPP
#define CALCULATOR_HPP
#include <vector>
#include <cmath>

template <typename T>
class Function
{
public:
    virtual ~Function(){};

    // calculates the value of the function represented by this class at the point x
    virtual T operator()(const T& x) = 0;
    // calculates the value of the function represented by this class at the point x (assuming the function is in C^(R))
    virtual T derivative(const T& x) = 0;
};

// an example child class
template <typename T>
class ConstantFunction : public Function<T>
{
public:
    ConstantFunction(const T& c) : c_(c) {}

    ConstantFunction() : ConstantFunction(0) {}

    // comment out the parameter to silent -Wunused-parameter warning
    T operator()(const T& /* x */)
    {
        return c_;
    }

    T derivative(const T& /* x */)
    {
        return 0;
    }

private:
    T c_;
};

template <typename T, int D>
class Polynomial : public Function<T>
{
public:
    // PRE: coeffs must be of size D+1 and contain the coefficients:
    // coeffs[0] + coeffs[1]*x + coeffs[2]*x^2 + ... + coeffs[D]*x^D
    Polynomial(const std::vector<T>& coeffs) : coeffs_(coeffs) {};

    T operator()(const T& x)
    {
        // An alternative way to evaluate polynomials is Horner's method
        // (without pow() function calls).
        T polyval = 0;
        for(int i = 0; i <= D; ++i)
        {
            polyval += coeffs_[i] * std::pow(x, i);
        }

        return polyval;
    }

    T derivative(const T& x)
    {
        // An alternative way to evaluate polynomials is Horner's method
        // (without pow() function calls).
        T deriv_val = 0;
        for(int i = 1; i <= D; ++i)
        {
            deriv_val += i * coeffs_[i] * std::pow(x, i-1);
        }

        return deriv_val;
    }

private:
    const std::vector<T> coeffs_;
};
#endif // CALCULATOR_HPP
