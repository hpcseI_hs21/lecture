import numpy as np
import torch
from torch import optim
from torch import nn
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
import sys


class LinearNN(nn.Module):

    def __init__(self, n, *hidden_sizes):
        super().__init__()

        # Add linear layers 
        sizes = [n] + list(hidden_sizes) + [n]
        layers = [nn.Linear(sizes[i], sizes[i + 1]) for i in range(len(sizes) - 1)]
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        return self.layers(x)


class ReLUNN(nn.Module):

    def __init__(self, n, *hidden_sizes):
        super().__init__()

        # Add linear and ReLU layers 
        sizes = [n] + list(hidden_sizes) + [n]
        layers_list = [nn.Linear(sizes[0], sizes[1])]
        for i in range(1, len(sizes) - 1):
          layers_list.append(nn.ReLU())
          layers_list.append(nn.Linear(sizes[i], sizes[i + 1]))
        self.layers = nn.Sequential(*layers_list)

    def forward(self, x):
        return self.layers(x)


def train_loop(X_train, X_val, model, loss, optimizer, wait_time, max_iter):

    i, no_progress, min_loss = 0, 0, sys.float_info.max
    while i < max_iter and no_progress < wait_time:
        
        # Take a step
        optimizer.zero_grad()
        loss(model(X_train), X_train).backward()
        optimizer.step()

        # Compute val loss
        val_loss = loss(model(X_val), X_val).item()

        # Test early stopping
        if val_loss < min_loss:
            min_loss, no_progress = val_loss, 0
        else:
            no_progress += 1
            
        i += 1



if __name__ == "__main__":

    # Load and split data
    data = np.loadtxt("faces.csv", delimiter=",")
    X_train, data_test = train_test_split(data, test_size=0.33, random_state=42)
    X_val, X_test = train_test_split(data_test, test_size=0.5, random_state=43)

    # Scale data
    scaler = StandardScaler()
    X_train = torch.from_numpy(scaler.fit_transform(X_train)).float()
    X_val = torch.from_numpy(scaler.transform(X_val)).float()
    X_test = torch.from_numpy(scaler.transform(X_test)).float()

    # Parameters
    N_tot, D = data.shape
    k = 128
    n_iter = 1000

    loss = nn.MSELoss()

    # PCA
    pca = PCA(n_components=k, svd_solver='auto').fit(X_train)
    X_test_compressed = pca.transform(X_test.numpy())
    X_test_reconstructed = torch.from_numpy(pca.inverse_transform(X_test_compressed))
    
    loss_pca = loss(X_test_reconstructed, X_test).item()
    print("PCA Recostruction Loss: ", loss_pca)

    # LinearNN
    modelLinear = LinearNN(D, k)
    optimizerLinear = optim.Adam(modelLinear.parameters(), lr=0.001)
    train_loop(X_train, X_val, modelLinear, loss, optimizerLinear, 10, n_iter)
    
    loss_linear = loss(modelLinear(X_test), X_test).item()
    print("Linear NN Recostruction Loss: ", loss_linear)

    # LinearNN Deep
    modelLinearDeep = LinearNN(D, k, k, k, k)
    optimizerLinearDeep = optim.Adam(modelLinearDeep.parameters(), lr=0.001)
    train_loop(X_train, X_val, modelLinearDeep, loss, optimizerLinearDeep, 10, n_iter)
    
    loss_linear_deep = loss(modelLinearDeep(X_test), X_test).item()
    print("Deep Linear NN Recostruction Loss: ", loss_linear_deep)

    # ReLU NN
    modelReLU = ReLUNN(D, k)
    optimizerReLU = optim.Adam(modelReLU.parameters(), lr=0.001)
    train_loop(X_train, X_val, modelReLU, loss, optimizerReLU, 10, n_iter)
    
    loss_relu = loss(modelReLU(X_test), X_test).item()
    print("ReLU NN Recostruction Loss: ", loss_relu)

    # ReLU NN Deep
    modelReLUDeep = ReLUNN(D, k)
    optimizerReLUDeep = optim.Adam(modelReLUDeep.parameters(), lr=0.001)
    train_loop(X_train, X_val, modelReLUDeep, loss, optimizerReLUDeep, 10, n_iter)
    
    loss_relu_deep = loss(modelReLUDeep(X_test), X_test).item()
    print("Deep ReLU NN Recostruction Loss: ", loss_relu_deep)

