from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

shape =(37, 50)

class FacialRecognitionSystem:

    def __init__(self, key_image : np.ndarray, reference_images : np.ndarray, k : int, eps : float) -> None:
        
        # Compute PCA
        self.pca = PCA(k, whiten=True)
        self.pca.fit(reference_images)

        # Compute compressed key
        if len(key_image.shape) == 1:
            key_image = key_image.reshape([1, -1])
        self.key = self.pca.transform(key_image)

        # Tolerance
        self.eps = eps


    def get_access(self, image : np.ndarray) -> bool:

        if len(image.shape) == 1:
            image = image.reshape([1, -1])

        # Grant access if L2-distance of compressed image
        # is less than the tolerance from the compressed key
        return np.linalg.norm(
            self.key - self.pca.transform(image)
        ) < self.eps


if __name__ == "__main__":

    data = np.loadtxt("faces.csv", delimiter=",")

    # Load key image
    key_filename = input("Input key file: ")
    key_raw = Image.open(key_filename).convert('L')
    key_resized = key_raw.resize(shape)
    key_array = np.array(key_resized.getdata())

    # Set up facial recognition system
    secure_system = FacialRecognitionSystem(key_array, data, 400, 50)

    # Test if images can be used to get access
    while True:
        filename = input("Input file to gain access: ")
        image_raw = Image.open(filename).convert('L')
        image_resized = image_raw.resize(shape)
        array = np.array(image_resized.getdata())
        
        if secure_system.get_access(array):
            print("Access granted!")
        else:
            print("Access denied!")
