# Introduction

In this tutorial we want to create a library with our innovative 
`matrix_io_write` algorithm that will help poor HPCSE I students to 
debug their code. In this step by step guide we will show how to build
a library as well as how to use it in production.

# 1

I want to make a library `matrix_io` with a function `matrix_io_write`. 
Here is a prototype of what `matrix_io_write` should do:

```c++
$ cat 1/main.cpp
#include <stdio.h>
int main()
{
    int m = 2, n = 3;
    double a[] = {1, -2, 3, -4, 5, -6};
    int i, j;
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++)
            printf("%-+.16e ", a[i * n + j]);
        printf("\n");
    }
}
```

```
$ g++ 1/main.cpp
$ ./a.out
+1.0000000000000000e+00 -2.0000000000000000e+00 +3.0000000000000000e+00
-4.0000000000000000e+00 +5.0000000000000000e+00 -6.0000000000000000e+00
```
# 2

Even in this simple case I have to make several design decisions: how to 
represent a matrix, should I print only to stdout, should printf specifier 
(`%-+.16e `) be a parameter, what to do on error. I settled on this:

```c++
$ cat 2/main.cpp
#include <stdio.h>
int matrix_io_write(FILE* file, int m, int n, double* a)
{
    int i, j;
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            if (fprintf(file, "%-+.16e ", a[i * n + j]) < 0)
                return 1;
        }
        if (fprintf(file, "\n") < 0)
            return 1;
    }
    return 0;
}

int main()
{
    int m = 2, n = 3;
    double a[] = {1, -2, 3, -4, 5, -6};
    matrix_io_write(stdout, m, n, a);
}
```
# 3

Here the code of the library `matrix_io_write` and of the client `main` are in the same file. In the following I split them:

```c++
$ cat 3/lib.cpp
#include <stdio.h>
int matrix_io_write(FILE*, int, int, double*);
int matrix_io_write(FILE* file, int m, int n, double* a)
{
    int i, j;
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            if (fprintf(file, "%-+.16e ", a[i * n + j]) < 0)
                return 1;
        }
        if (fprintf(file, "\n") < 0)
            return 1;
    }
    return 0;
}
```

and

```c++
$ cat 3/client.cpp
#include <stdio.h>
int matrix_io_write(FILE*, int, int, double*);
int main()
{
    int m = 2, n = 3;
    double a[] = {1, -2, 3, -4, 5, -6};
    if (matrix_io_write(stdout, m, n, a) != 0) {
        fprintf(stderr, "client: matrix_io_write failed\n");
        return 1;
    }
}
```

Now we can still compile all source files together to create an executable:
```
$ g++ 3/lib.cpp 3/client.cpp
$ ./a.out
+1.0000000000000000e+00 -2.0000000000000000e+00 +3.0000000000000000e+00
-4.0000000000000000e+00 +5.0000000000000000e+00 -6.0000000000000000e+00
```

# 4
Or the library can be compiled separatly and linked with the client.

```
$ g++ -c 4/lib.cpp
$ g++ 4/client.cpp lib.o
$ ./a.out
+1.0000000000000000e+00 -2.0000000000000000e+00 +3.0000000000000000e+00
-4.0000000000000000e+00 +5.0000000000000000e+00 -6.0000000000000000e+00
```

# 5
`matrix_io_write` must be declared in the client. I put the declaration in
a header file `matrix_io.h` which the client includes. A library can also
include `matrix_io.h` to be sure the declaration is consistent with
the definition.

```c++
$ cat 5/matrix_io.h
int matrix_io_write(FILE*, int, int, double *);
$ cat 5/lib.cpp
#include "matrix_io.h"
#include <stdio.h>
int matrix_io_write(FILE* file, int m, int n, double* a)
{
    int i, j;
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            if (fprintf(file, "%-+.16e ", a[i * n + j]) < 0)
                return 1;
        }
        if (fprintf(file, "\n") < 0)
            return 1;
    }
    return 0;
}
```

Since there will be several steps with dependencies I create a makefile

```
$ cat 5/Makefile
.PHONY: all
all: libmatrix_io.a libmatrix_io.so

libmatrix_io.a: lib.o
	ar r $@ lib.o
libmatrix_io.so: lib.o
	g++ -shared lib.o -o $@
lib.o: lib.cpp matrix_io.h
	g++ -c -fPIE lib.cpp
install: libmatrix_io.a
	mkdir -p ../include ../lib && \
	cp matrix_io.h ../include/ && \
	cp libmatrix_io.a ../lib/
	cp libmatrix_io.so ../lib/
```
It builds an objects file `lib.o` and puts it into an static and shared library `libmatrix_io.a` and `libmatrix_io.so` respectively. Usually a library contains several object files. `install` target copies files to a location where it is convenient for a client to find them.
```
$ (cd 5 && make install)
g++ -c lib.cpp
g++ -c -fPIE lib.cpp
ar r libmatrix_io.a lib.o
ar: creating libmatrix_io.a
g++ -shared lib.o -o libmatrix_io.so
mkdir -p ../include ../lib && \
cp matrix_io.h ../include/ && \
cp libmatrix_io.a ../lib/
cp libmatrix_io.so ../lib/
```
We can now examine our libraries a bit. By listing the content 
of the directory we can already make interesting observations.
If we compare the sizes of the two libraries we can see that our
shared library is a lot bigger. This is because the file also contains
symbols to dynamically load the library during runtime. Also the file
permission of the libraries are different. While the static library
has normal permissions as any other text file, the shared library
is an executable as one can see in the permissions column or the
green name of the executable.

```
$ ls -lah
total 36K
drwxr-xr-x. 1 user user  120 22. Okt 11:30 .
drwxr-xr-x. 1 user user  108 22. Okt 11:26 ..
-rw-rw-r--. 1 user user  357 22. Okt 02:23 lib.cpp
-rw-rw-r--. 1 user user 2.0K 22. Okt 11:30 libmatrix_io.a
-rwxrwxr-x. 1 user user  16K 22. Okt 11:30 libmatrix_io.so
-rw-rw-r--. 1 user user 1.8K 22. Okt 11:30 lib.o
-rw-rw-r--. 1 user user  316 22. Okt 02:23 Makefile
-rw-rw-r--. 1 user user   47 22. Okt 02:23 matrix_io.h
```
With the `ar` command we can verify that the static libary contains
the all the objects that we want. 
```
$ ar -t libmatrix_io.a
lib.o
```
With the command `nm` we can list all symbols our libaries contain.

```
nm libmatrix_io.*

libmatrix_io.a:

lib.o:
                 U fprintf
                 U _GLOBAL_OFFSET_TABLE_
0000000000000000 T _Z15matrix_io_writeP8_IO_FILEiiPd

libmatrix_io.so:
0000000000004028 b completed.0
                 w __cxa_finalize@GLIBC_2.2.5
0000000000001050 t deregister_tm_clones
00000000000010c0 t __do_global_dtors_aux
0000000000003de0 d __do_global_dtors_aux_fini_array_entry
0000000000003de8 d __dso_handle
0000000000003df0 d _DYNAMIC
00000000000011d0 t _fini
                 U fprintf@GLIBC_2.2.5
0000000000001100 t frame_dummy
0000000000003dd8 d __frame_dummy_init_array_entry
0000000000002088 r __FRAME_END__
0000000000004000 d _GLOBAL_OFFSET_TABLE_
                 w __gmon_start__
000000000000200c r __GNU_EH_FRAME_HDR
0000000000001000 t _init
                 w _ITM_deregisterTMCloneTable
                 w _ITM_registerTMCloneTable
0000000000001080 t register_tm_clones
0000000000004028 d __TMC_END__
0000000000001109 T _Z15matrix_io_writeP8_IO_FILEiiPd
```
As we can see both libraries contain the function matrix_io_write with
identical arguments.

# 6
To compile a client with the installed library the locations of the
header and of the archive must be set. `-I../include` tells the compiler
where to look for header files and `-L../lib/ -lmatrix_io` tells the
compiler where and and what libraries it has to look for In addition
to the default paths given by the environment variables. Usually
the compiler prefers to link against shared libraries to reduce memory
usage. Therefore we have to specify explicitly to link against the 
static libary with `../lib/libmatrix_io.a` as if it was an object file.

```c++
$ cat 6/client.cpp
#include <matrix_io.h>
#include <stdio.h>
int main()
{
    int m = 2, n = 3;
    double a[] = {1, -2, 3, -4, 5, -6};
    if (matrix_io_write(stdout, m, n, a) != 0) {
        fprintf(stderr, "client: matrix_io_write failed\n");
        return 1;
    }
}
```
If we now compile and execute the executables we notice that the
executable with the shared library fails this is because the path
to the shared object has to be declared in the environment variable
`$LD_LIBRARY_PATH` during runtime to load the required symbols.
```
$ cd 6
$ make
$ ./main_static
+1.0000000000000000e+00 -2.0000000000000000e+00 +3.0000000000000000e+00
-4.0000000000000000e+00 +5.0000000000000000e+00 -6.0000000000000000e+00
$ ./main_shared
./main_shared: error while loading shared libraries: libmatrix_io.so: cannot open shared object file: No such file or directory
$ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../lib/
$ ./main_shared 
+1.0000000000000000e+00 -2.0000000000000000e+00 +3.0000000000000000e+00 
-4.0000000000000000e+00 +5.0000000000000000e+00 -6.0000000000000000e+00 
```
We can once again examine the binaries. We notice that the executable
with the shared library is smaller than the one with static linking.
This is because in case of static linking the symbols are extracted 
from the archive and included in the executable.
```
ls -lah
total 1.6M
drwxr-xr-x. 1 user user   96 22. Okt 11:23 .
drwxr-xr-x. 1 user user  108 22. Okt 11:17 ..
-rw-rw-r--. 1 user user  253 22. Okt 02:23 client.cpp
-rw-rw-r--. 1 user user 2.1K 22. Okt 02:23 client.o
-rwxrwxr-x. 1 user user  24K 22. Okt 11:23 main_shared
-rwxrwxr-x. 1 user user 1.6M 22. Okt 11:23 main_static
-rw-rw-r--. 1 user user  262 22. Okt 11:23 Makefile
```

With `ldd` we can inspect against which libraries our executables
are linked. And indeed our dynamically linked executable lists our
library libmatrix_io.so while the statically linked does not.

```
$ ldd main*
main_shared:
        linux-vdso.so.1 (0x00007fff6f3d3000)
        libmatrix_io.so => ../lib/libmatrix_io.so (0x00007f5560fe2000)
        libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007f5560d9f000)
        libm.so.6 => /lib64/libm.so.6 (0x00007f5560c5b000)
        libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007f5560c40000)
        libc.so.6 => /lib64/libc.so.6 (0x00007f5560a71000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f5560fe9000)
main_static:
        not a dynamic executable
```

# 7

Finally, I add a function `matrix_io_read()` to `7/read.cpp` and put
`matrix_io_write()` into `7/write.cpp`, I also create a more flexible
Makefile:

```
$ cat 7/matrix_io.h
#include <stdio.h>
int matrix_io_write(FILE*, int, int, double*);
int matrix_io_read(FILE*, int*, int*, double**);
$ cat 6/Makefile
.SUFFIXES:
.SUFFIXES: .cpp .o
CXXFLAGS = -O2 -g
CXX = g++
PREFIX = $(HOME)/prefix
O = read.o write.o
L = libmatrix_io.a
H = matrix_io.h
all: $L
$L: $O
        ar r $L $O
.cpp.o:
        $(CXX) $< -c $(CXXFLAGS)
install: $L
        mkdir -p $(PREFIX)/include $(PREFIX)/lib && \
        cp $H $(PREFIX)/include && \
        cp $L $(PREFIX)/lib
clean:; -rm $L $O
```
`CXX`, `CXXFLAGS`, `PREFIX` are parameters. For example,
```
$ (cd 7 && make 'CXX = clang++' 'CXXFLAGS = -Wall -Wextra -O3 -g')
clang++ read.cpp -c -Wall -Wextra -O3 -g
clang++ write.cpp -c -Wall -Wextra -O3 -g
ar r libmatrix_io.a read.o write.o
ar: creating libmatrix_io.a
```
Install a library
```
$ (cd 7 && make clean install)
```

# blas
[blas/Makefile](blas/Makefile) compiles programs which use `matrix_io` and Intel MKL. A program `pkg-config` provides Intel MKL compilation flags.
```
$ cat blas/Makefile
.SUFFIXES:
.SUFFIXES: .cpp
PREFIX = $(HOME)/prefix
CXXFLAGS = -O2 -g
CXX = g++
MKL_FLAGS = `pkg-config --cflags --libs mkl-static-lp64-seq`
MATRIX_IO_FLAGS = -I$(PREFIX)/include -L$(PREFIX)/lib -lmatrix_io
M = p0 p1 p2
all: $M
.cpp:
	$(CXX) $< $(CXXFLAGS) $(LDFLAGS) $(MKL_FLAGS) $(MATRIX_IO_FLAGS) -o $@
clean:
	rm -f -- $M

```

I put several test matrices in files
```
$ cat blas/a
10, 20, 30
$ cat blas/b
1
2
3
$ cat blas/Am
1,2,3
4,5,6
$ cat blas/x
10
20
30
```

A program [blas/p0.cpp](blas/p0.cpp) takes a name of a function (dot or gemv) and two matrices and calls a corresponding MKL function.

```c++
$ cat blas/p0.cpp
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <vector>
#include <mkl.h>
#include <matrix_io.h>

int main(int argc, char** argv)
{
  enum {inc = 1};
  int ma, na, mb, nb;
  double *a, *b, alpha, beta;
  char *function;
  FILE *file;
  if (argc < 4) {
    fprintf(stderr, "p0: needs three arguments\n");
    return 1;
  }
  argv++;
  function = *argv++;
  if ((file = fopen(*argv, "r")) == NULL ||
      matrix_io_read(file, &ma, &na, &a) != 0) {
    fprintf(stderr, "p0: fail to read '%s'\n", *argv);
  }
  argv++;
  fclose(file);
  if ((file = fopen(*argv, "r")) == NULL ||
      matrix_io_read(file, &mb, &nb, &b) != 0) {
    fprintf(stderr, "p0: fail to read '%s'\n", *argv);
  }
  fclose(file);

  if (strcmp(function, "dot") == 0) {
    if (ma != 1 || na != mb || nb != 1) {
      fprintf(stderr, "p0: wrong dimensions for dot: %dx%d and %dx%d\n",
              ma, na, mb, nb);
      return 2;
    }
    printf("%g\n", cblas_ddot(na, a, inc, b, inc));
  } else if (strcmp(function, "gemv") == 0) {
    if (na != mb  || nb != 1) {
      fprintf(stderr, "p0: wrong dimensions for dgemv: %dx%d and %dx%d\n",
              ma, na, mb, nb);
      return 2;
    }
    std::vector<double> y(na * nb);
    alpha = 1.0;
    beta = 0.0;
    cblas_dgemv(CblasRowMajor, CblasNoTrans, ma, na, alpha,
                a, na, b, inc, beta, y.data(), inc);
    if (matrix_io_write(stdout, ma, 1, y.data()) != 0)
      return 2;
  } else {
    fprintf(stderr, "p0: unknown function '%s'\n", function);
    return 2;
  }
  free(b);
  free(a);
  mkl_finalize();
}
```

For example,
```
$ cd blas
$ module load new intel/2018.1
$ make
g++ p0.cpp -O2 -g  `pkg-config --cflags --libs mkl-static-lp64-seq` -I/cluster/home/lisergey/prefix/include -L/cluster/home/lisergey/prefix/lib -lmatrix_io -o p0
g++ p1.cpp -O2 -g  `pkg-config --cflags --libs mkl-static-lp64-seq` -I/cluster/home/lisergey/prefix/include -L/cluster/home/lisergey/prefix/lib -lmatrix_io -o p1
g++ p2.cpp -O2 -g  `pkg-config --cflags --libs mkl-static-lp64-seq` -I/cluster/home/lisergey/prefix/include -L/cluster/home/lisergey/prefix/lib -lmatrix_io -o p2
$ ./p0 dot a b
140
```

If MKL_VERBOSE is set to 1 the calls of MKL functions are traced:

```
$ MKL_VERBOSE=1 ./p0 gemv Am x
MKL_VERBOSE Intel(R) MKL 2020.0 Product build 20191122 for Intel(R) 64 architecture Intel(R) Streaming SIMD Extensions 4.2 (Intel(R) SSE4.2) enabled processors, Lnx 1.60GHz lp64 sequential
MKL_VERBOSE DGEMV(T,3,2,0x7fffb2f9c980,0x55b2fcafc160,3,0x55b2fcafc0a0,1,0x7fffb2f9c988,0x55b2fcafc0c0,1) 53.50us CNR:OFF Dyn:1 FastMM:1 TID:0  NThr:1
+1.4000000000000000e+02
+3.2000000000000000e+02
```

The programs [blas/p1.cpp](blas/p1.cpp) and [blas/p2.cpp](blas/p2.cpp) compare performance of MKL functions with the manual implementations.

```
$ ./p2
Size: 16384
MFlops Executed: 512.00
MBytes Accessed: 8192.00
Operational Intensity: 0.062500
Manual:
    Result: 1.5657096575000000e+08
    Computation Time: 3.345s
    GFlops/s:  0.149491
BLAS:
    Result: 1.5657096575000000e+08
    Computation Time: 0.157s
    GFlops/s:  3.180541
Performance Ratio: 4.70%
```
One can try different compilers and flags. For example,
```
$ module load llvm
$ make clean
$ make 'CXXFLAGS = -O3 -march=native' 'CXX = clang++'
```
or
```
$ module load intel
$ make clean
$ make 'CXXFLAGS = -O3 -march=native' 'CXX = icpc'
```

# gemv
Also consider Link Line Advisor. This site recommends how to link your program against Intel MKL. For example to try out different threading models, etc. A program in [gemv/gemv.cpp](gemv/gemv.cpp) compete with Intel MKL using different approaches.

# References
- [intel:mkl_malloc](https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/support-functions/memory-management/mkl-malloc.html)
- [intel:cblas_ddot](https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/blas-and-sparse-blas-routines/blas-routines/blas-level-1-routines-and-functions/cblas-dot.html)
- [intel:cblas_dgemv](https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/blas-and-sparse-blas-routines/blas-routines/blas-level-2-routines/cblas-gemv.html)
- [intel:cblas_dgemm](https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/blas-and-sparse-blas-routines/blas-routines/blas-level-3-routines/cblas-gemm.html)
- [intel:dsecnd](https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-fortran/top/support-functions/timing/second-dsecnd.html)
- [Link Line Advisor](https://software.intel.com/content/www/us/en/develop/articles/intel-mkl-link-line-advisor.html)
- `$ man pkg-config`
