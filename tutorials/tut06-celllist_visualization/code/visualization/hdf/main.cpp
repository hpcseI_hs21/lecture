#include <algorithm>
#include <array>
#include <cmath>
#include <fstream>
#include <hdf5.h>
#include <iostream>
#include <mpi.h>
#include <sstream>
#include <stdio.h>
#include <vector>

struct Field {
    std::array<size_t, 3> dims; // global size in cells (x,y,z)
    size_t x0, x1;              // local range in x
    double h;                   // cell size
    std::array<double, 3> origin;
    std::vector<double> buf;
};

Field get_field(size_t nx, size_t ny, size_t nz, int rank, int commsize, int t)
{
    size_t x0 = rank * nx / commsize;
    size_t x1 = (rank + 1) * nx / commsize;
    Field u;
    u.dims = {nx, ny, nz};
    u.x0 = x0;
    u.x1 = x1;
    u.h = 3. / nx;
    u.origin = {-u.h * u.dims[0] * 0.5, -u.h * u.dims[1] * 0.5, 0};
    u.buf.resize((u.x1 - u.x0) * u.dims[1] * u.dims[2]);
    const double rad = 0.2;
    // helix
    int i = 0;
    for (size_t iz = 0; iz < nz; ++iz) {
        for (size_t iy = 0; iy < ny; ++iy) {
            for (size_t ix = x0; ix < x1; ++ix) {
                double x = u.origin[0] + (ix + 0.5) * u.h;
                double y = u.origin[1] + (iy + 0.5) * u.h;
                double z = u.origin[2] + (iz + 0.5) * u.h;
                double phi = std::atan2(y, x);
                double xp = std::cos(phi);
                double yp = std::sin(phi);
                double dz = phi / (2 * M_PI) - t * 0.1 - (z - std::ceil(z));
                double dzz = std::min(std::min(std::abs(dz), std::abs(dz - 1)),
                                      std::abs(dz + 1));
                u.buf[i++] =
                    rad - std::sqrt(std::pow(xp - x, 2) + std::pow(yp - y, 2) +
                                    std::pow(dzz, 2));
            }
        }
    }
    return u;
}

void write_xmf(std::string xmfpath, std::string hdfpath, std::string fieldname,
               std::array<size_t, 3> dims, std::array<double, 3> origin,
               std::array<double, 3> spacing)
{
    std::ofstream f(xmfpath);
    f << "<?xml version='1.0' ?>\n"
      << "<!DOCTYPE Xdmf SYSTEM 'Xdmf.dtd' []>\n"
      << "<Xdmf Version='2.0'>\n"
      << " <Domain>\n"
      << "   <Grid GridType='Uniform'>\n"
      << "     <Topology TopologyType='3DCORECTMesh' Dimensions='"
      // number of nodes
      << dims[2] + 1 << ' ' << dims[1] + 1 << ' ' << dims[0] + 1 << "'/>\n\n"
      << "     <Geometry GeometryType='ORIGIN_DXDYDZ'>\n"
      << "       <DataItem Name='Origin' Dimensions='3' NumberType='Float' "
         "Precision='8' Format='XML'>\n"
      << "     " << origin[2] << ' ' << origin[1] << ' ' << origin[0] << '\n'
      << "       </DataItem>\n"
      << "       <DataItem Name='Spacing' Dimensions='3' NumberType='Float' "
         "Precision='8' Format='XML'>\n"
      << "     " << spacing[2] << ' ' << spacing[1] << ' ' << spacing[0] << '\n'
      << "       </DataItem>\n"
      << "     </Geometry>\n\n"
      << "     <Attribute Name='" << fieldname
      << "' AttributeType='Scalar' Center='Cell'>\n"
      << "       <DataItem Dimensions='"
      // number of cells
      << dims[2] << ' ' << dims[1] << ' ' << dims[0]
      << "' NumberType='Float' Precision='8' Format='HDF'>\n"
      << "        " << hdfpath << ":/data\n"
      << "       </DataItem>\n"
      << "     </Attribute>\n"
      << "   </Grid>\n"
      << " </Domain>\n"
      << "</Xdmf>\n";
}

void write_hdf(std::string hdfpath, const Field& u, MPI_Comm comm)
{
    const int kDim = 3;
    hid_t fapl;

    fapl = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(fapl, comm, MPI_INFO_NULL);
    // Create file
    hid_t file = H5Fcreate(hdfpath.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
    H5Pclose(fapl);

    hsize_t count[kDim] = {u.dims[2], u.dims[1], u.x1 - u.x0}; // local size
    hsize_t dims[kDim] = {u.dims[2], u.dims[1], u.dims[0]};    // global size
    hsize_t offset[kDim] = {0, 0, u.x0};

    fapl = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(fapl, H5FD_MPIO_COLLECTIVE); // enable MPI IO
    // Create file space
    hid_t fspace = H5Screate_simple(kDim, dims, NULL);
    // Create dataset
    hid_t dataset = H5Dcreate(file, "data", H5T_NATIVE_DOUBLE, fspace,
                              H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    // Select slab in filespace
    H5Sselect_hyperslab(fspace, H5S_SELECT_SET, offset, NULL, count, NULL);
    // Create memory space
    hid_t mspace = H5Screate_simple(kDim, count, NULL);
    // Write to file
    H5Dwrite(dataset, H5T_NATIVE_DOUBLE, mspace, fspace, fapl, u.buf.data());
    H5Pclose(fapl);

    H5Sclose(fspace);
    H5Sclose(mspace);
    H5Dclose(dataset);
    H5Fclose(file);
}

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm comm = MPI_COMM_WORLD;

    int size, rank;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    const int tmax = 9;
    for (int t = 0; t <= tmax; ++t) {
        if (rank == 0) {
            std::cout << "t=" << t << std::endl;
        }
        auto nx = 64;
        auto u = get_field(nx, nx, nx * 3 / 2, rank, size, t);
        const auto st = std::to_string(t);
        const std::string hdfpath = "grid_" + st + ".h5";
        const std::string xmfpath = "grid_" + st + ".xmf";
        const std::array<double, 3> spacing{u.h, u.h, u.h};
        write_hdf(hdfpath, u, comm);
        write_xmf(xmfpath, hdfpath, "u", u.dims, u.origin, spacing);
    }

    MPI_Finalize();
}
